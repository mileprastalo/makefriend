package com.makefriend.makefriend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MakefriendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MakefriendApplication.class, args);
	}

}
